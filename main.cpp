#include <iostream>
#include <regex>
#include <string>
#include <set>
#include <vector>
#include <fstream>
#include <sstream>
#include "../lab1flex/Container.h"
#include "../lab1flex/Aim.h"
#include "../lab1flex/generator.h"

int getInt(int* a) {
	int n = 0;
	do {
		n = scanf("%d", a);
		if (n == 0) {
			printf("Error! Type integer again\n>> ");
			scanf("%*[^\n]");
		}
	} while (n == 0);
	scanf("%*c");
	return n < 0 ? 1 : 0;
}

void readffile(std::ifstream& fin) {
	std::string filename;
	std::string error = "";
	do {
		std::cout << error << "Please enter the name of the file: ";
		std::cin >> filename;
		fin.open(filename);
		error = "Error with opening \"" + filename + "\". Try again\n";
	} while (!(fin.is_open()));
}

std::set <std::string> postProc(std::vector <Aim> init) {
	std::set <std::string> names, resreqs;
	std::vector <std::string> reqs;
	for (auto it = init.begin(); it != init.end(); it++) {
		if (names.find((*it).getName()) == names.end()) {
			names.insert((*it).getName());
		}
		else {
			continue;
		}
	}
	for (auto it = init.begin(); it != init.end(); it++) {
		reqs = (*it).gerReq();
		for (auto it1 = reqs.begin(); it1 != reqs.end(); it1++) {
			if (names.find(*it1) == names.end()) resreqs.insert(*it1);
		}
	}
	return resreqs;
}

void regexProc(std::string name, std::string reqs) {
    Container::addAim(name);
    std::vector <std::string> reqvec;
    std::string word;
    std::stringstream s(reqs);
    while (s >> word) reqvec.push_back(word);
    for (auto it = reqvec.begin(); it != reqvec.end(); it ++) {
        if (Container::addReqAim(*it)) {
            Container::delAim();   
            break;
        }
        else continue;
    }
}

int main() {
    std::cmatch resultregex;
    std::regex regular ("(([a-zA-Z(\.)(\_)])([a-zA-Z0-9(\.)(\_)])*)"
                        "([ \t]*)"
                        "(:)"
                        "([ \t]*)"
                        "((([a-zA-Z(\.)(\_)])([a-zA-Z0-9(\.)(\_)])*([ \t])*)*)"
                        "((\n){0,1})"
                        );
    int mode = 0;
	std::set <std::string> resreqs;
	Container::index = -1;
	Container::result.clear();
	std::cout << "Please select a working mode:\n - Input from file (1)\n - Input from console (2)\n>> ";
	while (getInt(&mode) || (mode != 1 && mode != 2)) std::cout << "Incorrect input. Please try again!" << std::endl << ">> ";
	if (mode == 1) {
        std::string temp;
		std::ifstream fin;
		int flag;
		std::cout << "Would you like to generate random file? (Yes - 1, No - 0)\n>> ";
		while (getInt(&flag) || (flag != 0 && flag != 1)) std::cout << "Incorrect input. Please try again!" << std::endl << ">> ";
		if (!flag) {
			readffile(fin);
		}
		else {
			generator::generate("GeneratedTest.txt", 1);
			fin.open("GeneratedTest.txt");
		}
        while(true) {
        std::getline(fin, temp);
        if(!fin.eof()) {
            if (std::regex_match(temp.c_str(), resultregex, regular)) regexProc(resultregex[1], resultregex[7]);
        }
        else break;
        }
		fin.close();
	}
	else if (mode == 2) {
		std::string temp = "";
		std::cout << "Type strings below (type \"/end\" to complete the input): \n";
		do {
			std::getline(std::cin, temp);
			if (std::regex_match(temp.c_str(), resultregex, regular)) regexProc(resultregex[1], resultregex[7]);
		} while (temp != "/end");
	}
	auto result = Container::getVec();
	resreqs = postProc(result);
	std::cout << "\nCorrect strings: \n";
	for (auto it = result.begin(); it != result.end(); it++) (*it).showFull();
	std::cout << "\nGoals that respond conditions: \n";
	for (auto it = resreqs.begin(); it != resreqs.end(); it++) std::cout << ((it == resreqs.begin()) ? (*it) : (", " + *it));
	std::cout << std::endl;
    
    return 0;
}